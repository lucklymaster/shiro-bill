package entity;

import java.util.List;
/**
 * 
 * @author yoyo198212
 *  layui需要的返回数据
 * @param <T>
 */
public class LayuiEntity<T> {

	private Integer code = 0;
	private String msg = "" ;
	private Integer count ;
	private List<T> data ; //不是date
	
	
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public List<T> getData() {
		return data;
	}
	public void setData(List<T> data) {
		this.data = data;
	}
	
	
}
