package entity;

/**
 * 条件对象,生成动态sql
 * @author yoyo198212
 *
 */
public class Criteria {

	private Integer id ;  //update的时候进行id查询
	private String productName ;  //订单名称模糊查询
	private Integer start ; //分页的2个参数
	private Integer offset ;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Integer getStart() {
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public Integer getOffset() {
		return offset;
	}
	public void setOffset(Integer offset) {
		this.offset = offset;
	}
	
	
	
}








