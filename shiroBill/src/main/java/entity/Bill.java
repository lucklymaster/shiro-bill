package entity;

import java.util.Date;
/**
 * 订单表
 * @author yoyo198212
 *
 */
public class Bill {
	private Integer id ;       //    bigint(20)       主键ID                                       
	private String billCode    ;//  varchar(20)    账单编码                                   
	private String productName ;//  varchar(20)    商品名称                                   
	private String productDesc ;//  varchar(50)     商品描述                                   
	private String productUnit ;//  varchar(10)    商品单位                                   
	private Double productCount ;// decimal(20,2)   商品数量                                   
	private Double totalPrice ;//   decimal(20,2)   商品总额                                   
	private Integer isPayment  ;//    int(10)       是否支付（1：未支付 2：已支付）  
	private Integer createdBy  ;//    bigint(20)  创建者（userId）                          
	private Date creationDate ;//  datetime        创建时间                                   
	private Integer modifyBy  ;//    bigint(20)     更新者（userId）                          
	private Date modifyDate ;//   datetime       更新时间                                   
	private Integer providerId  ;//  int(20)  
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getBillCode() {
		return billCode;
	}
	public void setBillCode(String billCode) {
		this.billCode = billCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductDesc() {
		return productDesc;
	}
	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}
	public String getProductUnit() {
		return productUnit;
	}
	public void setProductUnit(String productUnit) {
		this.productUnit = productUnit;
	}
	public Double getProductCount() {
		return productCount;
	}
	public void setProductCount(Double productCount) {
		this.productCount = productCount;
	}
	public Double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public Integer getIsPayment() {
		return isPayment;
	}
	public void setIsPayment(Integer isPayment) {
		this.isPayment = isPayment;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Integer getModifyBy() {
		return modifyBy;
	}
	public void setModifyBy(Integer modifyBy) {
		this.modifyBy = modifyBy;
	}
	public Date getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	public Integer getProviderId() {
		return providerId;
	}
	public void setProviderId(Integer providerId) {
		this.providerId = providerId;
	}
	
	
	
}
