package service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import dao.BillDAO;
import entity.Bill;
import entity.Criteria;
import service.BillService;

@Component
public class BillServiceImpl implements BillService{
	
	@Autowired
	private BillDAO dao ;

	@Override
	public List<Bill> findBillCriteria(Criteria cr) {
		return dao.findBillCriteria(cr);
	}

	@Override
	public void addBill(Bill b) {
		dao.addBill(b);
	}

	@Override
	public void updateBill(Bill b) {
		dao.updateBill(b);
	}

	@Override
	public void deleteBill(Integer id) {
		dao.deleteBill(id);
	}

	@Override
	public Integer getCount() {
		return dao.getCount();
	}

}
