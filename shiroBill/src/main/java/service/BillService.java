package service;

import java.util.List;

import entity.Bill;
import entity.Criteria;

public interface BillService {
	public List<Bill> findBillCriteria(Criteria cr);
	
	public void addBill(Bill b) ;
	
	public void updateBill(Bill b) ;
	
	public void deleteBill(Integer id) ;
	
	public Integer getCount();
}
