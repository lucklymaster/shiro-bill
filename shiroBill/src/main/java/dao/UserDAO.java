package dao;

import java.util.List;

import entity.User;
import entity.UserRolePermission;

public interface UserDAO {

	public User findUser(String name);
	
	public User findPhoneUser(String phone);
	
	public List<UserRolePermission> 
							findUserRolePermission(String name);
}
 