package dao;

import java.util.List;

import entity.Bill;
import entity.Criteria;

public interface BillDAO {
	/**
	 * 条件查询  所有的功能都放在1个页面上
	 * @param cr
	 * @return
	 */
	public List<Bill> findBillCriteria(Criteria cr);
	/**
	 * 添加
	 * @param b
	 */
	public void addBill(Bill b) ;
	
	public void updateBill(Bill b) ;
	
	public void deleteBill(Integer id) ;
	
	//统计行数  分页
	public Integer getCount();
}





