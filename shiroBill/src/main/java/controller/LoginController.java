package controller;


import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class LoginController {

	/**
	 * 认证的代码
	 * @param token
	 * @return
	 */
	@RequestMapping("login")
	@ResponseBody
	public String login(UsernamePasswordToken token){
		//得到当前线程对象
		Subject subject=SecurityUtils.getSubject();
		
		try {
			//登录 判断是否登录成功
			subject.login(token);
			//登录成功将当前用户存入session
			Session session=subject.getSession();
			
			session.setAttribute("user", token);
			System.out.println(token);
			return "success";
		} catch (Exception e) {
			e.printStackTrace();
			return "fail";
		}
	}
	

	
}
