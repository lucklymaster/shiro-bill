package controller;

import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import entity.Bill;
import entity.Criteria;
import entity.LayuiEntity;
import service.BillService;
import service.impl.BillServiceImpl;

@Controller
public class BillController {

	@Autowired
	private BillService service ;
	
	/**
	 * 
	 * @param page layui加载页面或点击下标数字会发送的参数
	 * @param limit  layui发送的参数  每页多少条默认10
	 */
	@RequestMapping("loadLayuiTable")
	@ResponseBody
	public LayuiEntity<Bill> loadLayuiTable(@RequestParam(defaultValue="1") Integer page,
			                    Integer limit ,
			                    String productName  ,  //模糊查询
			                    Integer id)   //id查询
	
	{
		//计算分页
		Integer offset = 5 ; //默认每页5条
		Integer start = (page-1) * 5; 
		//创建条件
		Criteria cr = new Criteria();
		cr.setId(id);
		cr.setProductName(productName);
		cr.setStart(start);
		cr.setOffset(offset);
		
		//执行查询
		List<Bill> list = service.findBillCriteria(cr);
		//统计行数
		Integer count = service.getCount();
		//包装layui需要的数据进行返回
		//{"code":0,"msg":"","count":1000,"data":[{"id":10000,"usernam...
		LayuiEntity<Bill> layui = new LayuiEntity<Bill>();
		layui.setCount(count);
		layui.setData(list);
		return layui;
	}
	
	//修改表单提交的url
	@RequestMapping("updateBill")
	@ResponseBody
	public String updateBill(Bill bill){
		try{
			service.updateBill(bill);
			return "success";
		}catch(Exception e){
			return "fail" ;
		}
	}
	
	//添加订单到数据库
	@RequestMapping("addBill")
	@ResponseBody
	public String addBill(Bill bill){
		try{
			service.addBill(bill);
			return "success";
		}catch(Exception e){
			return "fail" ;
		}
	}
	
	//根据id删除表格的行
	@RequestMapping("deleteBillRow")
	@ResponseBody
	public String deleteBillRow(Integer id){
		//进行权限判断，管理员才能删除		
		Subject subject=SecurityUtils.getSubject();
		
		if(subject.hasRole("管理员")){
			
			service.deleteBill(id);
			return "success";
		}else{
			return "fail";
		}
	}
	
	
}







