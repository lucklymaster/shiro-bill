package realms;

import java.util.LinkedHashMap;

public class FilterChainDefinitionMapBuilder {

	public LinkedHashMap<String, String> buildFilterChainDefinitionMap(){
		LinkedHashMap<String, String> map = new LinkedHashMap<>();
		
		map.put("/statics/admin.jsp", "anon");   //通过登录或记住我
		map.put("/statics/logout", "logout");   //退出登录
		map.put("/statics/gly.jsp", "authc,roles[管理员]");//角色管理员才能访问
		map.put("/statics/tj.jsp", "authc,perms[添加]"); //有添加权限才能访问
		
		
		/*   对于页面的拦截
		 *     anon  匿名访问
		 *     authc  需要登录才能访问
		 * 	   user  需要登录或 通过记住我
		 *    roles[admin] 需要admin角色才能访问的页面
		 *    logout 退出登录
		 *     其他页面没有配的都能匿名访问
		 * 
		 * 
		 * */
		
		
		//其他页面需要认证
		//map.put("/statics/admin/**", "authc");
		
		return map;
	}
}
