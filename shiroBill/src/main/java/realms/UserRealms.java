package realms;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.ibatis.session.SqlSession;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import dao.UserDAO;
import entity.User;
import entity.UserRolePermission;


public class UserRealms extends AuthorizingRealm{

	@Autowired 
	private UserDAO dao;
	//授权
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(
			PrincipalCollection principals) {
		//查询subject的角色的的权限，不需要密码 只需要用户名
		String username=(String)principals.getPrimaryPrincipal();
		//查询数据库，得到list集合
		List<UserRolePermission> list=dao.findUserRolePermission(username); 
		//角色集合
		Set<String> rolesSet=new HashSet<String>();
		//权限集合
		Set<String> permissionSet=new HashSet<String>();
		//获得角色的数据
		for(UserRolePermission urp:list){
			String  role=urp.getRname();
			String permission =urp.getPname();
			rolesSet.add(role);
			permissionSet.add(permission);
		}
		//创建返回数据  分别将角色权限添加到info中
		SimpleAuthorizationInfo info=new SimpleAuthorizationInfo();
		//将角色，权限添加到info
		info.addRoles(rolesSet);
		info.addStringPermissions(permissionSet);
		return info;
	}

	//认证
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(
			AuthenticationToken token) throws AuthenticationException {
		// TODO Auto-generated method stub
		System.out.println("自定义reaml被执行");
		//得到username
		String username = (String)token.getPrincipal();
		User user = dao.findUser(username);
		String password =user.getPassword();
		//将查询到的结果和username进行包装并返回
				
		SimpleAuthenticationInfo info = 
				new SimpleAuthenticationInfo(username, password,getName());
		return info;

	}

	
}
