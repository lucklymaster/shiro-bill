<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>订单管理</title>
    <!-- 导入样式 -->
    <link rel="stylesheet" type="text/css" href="https://www.layuicdn.com/layui/css/layui.css" />
 	<!-- 导入 jquery -->
 	<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
 	<link rel="stylesheet" href="/shiroBill/statics/css/layui_layout.css" />
 	
  </head>
  <body>
  	 <fieldset class="layui-elem-field layui-field-title site-title">
      <legend><a name="compatibility">超市订单管理系统</a></legend>
     </fieldset>
  	 <!-- 放一个表格容器 -->
 	 <table id="demo"  lay-filter="demo"></table>
  	
  	 <!-- 修改表单 -->
  	 <form class="layui-form" action="">
  	 	<div class="layui-form-item">
		    <label class="layui-form-label">商品编码</label>
		    <div class="layui-input-block">
		      <input type="text" name="billCode"  class="layui-input">
		    </div>
		</div>
		<div class="layui-form-item">
		    <label class="layui-form-label">商品名称</label>
		    <div class="layui-input-block">
		      <input type="text" name="productName"  class="layui-input">
		    </div>
		</div>
		<div class="layui-form-item">
		    <label class="layui-form-label">商品描述</label>
		    <div class="layui-input-block">
		      <input type="text" name="productDesc"  class="layui-input">
		    </div>
		</div>
		<div class="layui-form-item">
		    <label class="layui-form-label">商品单位</label>
		    <div class="layui-input-block">
		      <input type="text" name="productUnit"  class="layui-input">
		    </div>
		</div>
		
		<input type="text" name="id" />
		
		<div class="layui-form-item">
		    <div class="layui-input-block">
		      <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
		      <button type="reset" class="layui-btn layui-btn-primary">重置</button>
		    </div>
		</div>
  	 </form>
  	
  	
  	 <!-- 数据表格工具条 -->
  	 <div id="tools" style="display:none;">
  	 	<button id="quanxuan" class="layui-btn layui-btn-warm layui-btn-xs">全选</button>
  	 	<button id="fanxuan" class="layui-btn layui-btn-xs">反选</button>
  	 	<button id="shanchu" class="layui-btn layui-btn-xs">删除</button>
  	 	<div class="layui-input-inline">
  	 		<input type="text" name="search" class="layui-input" />
  	 	</div>
  	 	<button id="sousuo" class="layui-btn layui-btn-danger layui-btn-xs">搜索</button>
  	 </div>
  
  	 <!-- 表格行工具按钮 -->
  	 <div id="gj" style="display:none;">
  		 <button  class="layui-btn layui-btn-danger layui-btn-xs" 
  		 				 lay-event="del">删除</button>
  	 	 <button  class="layui-btn layui-btn-xs" 
  	 	 				 lay-event="edit">修改</button>
  	 </div>
  
  <!-- 导入layui js依赖 -->
  <script src="https://www.layuicdn.com/layui/layui.all.js"></script>
  </body>
</html>

<script>
//加载table模块
layui.use(['jquery','element','table','form','layer'],function(){
    var table = layui.table ;
    var $ = layui.jquery ;
    var form = layui.form ;
    var layer = layui.layer ;
     
     //创建表格参数
	var obj = {
		elem: '#demo'  //id选择器
	   ,url: '/shiroBill/loadLayuiTable'   //数据接口,某个Controller
	   ,page: true //开启分页
	   ,toolbar:'#tools' //开启头部工具条
	   ,limit:5  //每页显示多少条
	   ,cols: [[ //表头  表格的标题列
	   		{type:'checkbox'}
		   ,{field: 'id', title: '编号',width:30}
		   ,{field: 'billCode', title: '编码'}
     	   ,{field: 'productName', title: '订单名称'}
     	   ,{field: 'productDesc', title: '订单描述'}
     	   ,{field: 'productUnit', title: '单位'}
     	   ,{field: 'productCount', title: '数量'}
     	   ,{field: 'totalPrice', title: '总计'}
     	   ,{field: 'isPayment', title: '是否支付'}
     	   ,{field: 'createdBy', title: '创建'}
     	   ,{field: 'creationDate', title: '订单描述'}
     	   ,{field: 'providerId', title: '修改者'}
     	   ,{toolbar:'#gj'}  //添加行工具
     	   
	   ]]
	} ;
	//加载表格
	table.render(obj);
     
    //头工具的事件
    $("#sousuo").click(function(){
    	//得到文本框
    	var name = $("input[name=search]").val();
    	
    	//直接调用表格的reload方法 他会帮你发送ajax请求与拼接表格
    	table.reload('demo',{
    		url:'/shiroBill/loadLayuiTable',
    		where:{'productName':name ,'page':1}
    	});
    	
    	//重新加载页面
    	//window.location.reload(true);
    	
    });
     
     //点击修改按钮的事件
     //监听行工具事件  给表格加上 lay-filter="test"
	table.on('tool(demo)', function(obj){
		 var data = obj.data;  //将行的数据进行赋值
		 if(obj.event === 'del'){ //点了删除按钮
		 	alert(data.id + ","+data.username);   //得到行的数据，发送ajax
		 }else if(obj.event === 'edit'){
		 	//点击行工具修改，将数据加载到表单
		 	$("input[name=billCode]").val(data.billCode);
		 	$("input[name=productName]").val(data.productName);
		 	$("input[name=productDesc]").val(data.productDesc);
		 	$("input[name=productUnit]").val(data.productUnit);
		 	$("input[name=id]").val(data.id);
		 }
	});
	
	//点击表单提交按钮
	form.on('submit(formDemo)', function(data){
	  console.log(data.field) //当前容器的全部表单字段，名值对形式：{name: value}
	  //得到所有的文本框
	  var id = data.field.id ;
	  var billCode = data.field.billCode ;
	  var productName = data.field.productName ;
	  var productDesc = data.field.productDesc ;
	  var productUnit = data.field.productUnit ;
	 //拼接数据
	  var obj = {
	  	"id":id ,
	  	"billCode":billCode ,
	  	"productName":productName ,
	  	"productDesc":productDesc ,
	  	"productUnit":productUnit 
	  } ;
	   //发送ajax请求
	  $.post('/layuiBill/updateBill',obj,function(data){
	  		layer.alert(data);
	  },'text');
	
	  return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
	});
	
	
     

});

</script>







